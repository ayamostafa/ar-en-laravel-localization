<?php
/**
 * Created by PhpStorm.
 * User: Aya_Dream
 * Date: 4/30/2018
 * Time: 4:52 PM
 */

return [
    'title' => 'Laravel',
    'btn' => 'Language',
    'arabic' => 'Arabic',
    'english' => 'English'
];